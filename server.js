var net = require('net');

// host adress
var host = '127.0.0.1';
// timeout in ms (1000ms = 1s)
var timeout = 5000;
// scanner mode
var mode = 'RANGE'; // RANGE / LIST
// port range
var start = 0;
var end = 1023;
// port liste
var portList = [
  80,
  81,
  83,
  84,
  85,
  86,
  87
];

if (mode == 'LIST') {
  var portListLength = portList.length;
  for (var i = 0; i < portListLength; i++) {
    var port = portList[i];
    portScanner(port);
  }
} else if (mode == 'RANGE') {
  while (start <= end) {
    var port = start;
    portScanner(port);
    start++;
  }
}

function portScanner(port) {
  //console.log('CHECK: ' + port);
  var s = new net.Socket();

  s.setTimeout(timeout, function() {
    s.destroy();
  });

  s.connect(port, host, function() {
    console.log('OPEN: ' + port);
  });

  // if any data is written to the client on connection, show it
  s.on('data', function(data) {
    console.log(port + ': ' + data);
    s.destroy();
  });

  s.on('error', function(e) {
    if (e.code == "ECONNREFUSED") {
      console.log('CLOSED: ' + e.port);
    }
    s.destroy();
  });
}
