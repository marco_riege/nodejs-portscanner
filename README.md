Summary
==================

This repository is for the NodeJS Portscanner project. An litte application to scan a range or list of ports.

How to install and run the project:
==================
1. `git clone`
2. cd `nodejs-portscanner/`
3. `node server.js`

Settings
==================
You can find the settings at the top of the portscanner.js file:

1. host
2. timeout
3. mode (LIST/RANGE)
4. start / end (for RANGE mode)
5. portList (for LIST mode)
